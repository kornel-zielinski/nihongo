package desu.nihongo;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import java.io.File;
import java.io.FileNotFoundException;

public class ChooseFileActivity extends AppCompatActivity {
    private ListModel wordListModel;

    private TextView textFirst;
    private TextView textLast;
    private TextView textTotal;
    private EditText textStart;
    private EditText textEnd;

    private static int REQUEST_GET_FILE = 101;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_file);
        Log.i(LearningActivity.TAG, "ChooseFileActivity starts");
        setTitle(R.string.title_choose_file);

        textFirst = findViewById(R.id.textFirst);
        textLast = findViewById(R.id.textLast);
        textTotal = findViewById(R.id.textTotal);
        textStart = findViewById(R.id.textStart);
        textEnd = findViewById(R.id.textEnd);

        textStart.setOnEditorActionListener(new CustomTextViewListener());
        textEnd.setOnEditorActionListener(new CustomTextViewListener());

        Intent intent = new Intent(Intent.ACTION_GET_CONTENT)
                .addCategory(Intent.CATEGORY_OPENABLE)
                .setType("text/plain");
        startActivityForResult(intent, REQUEST_GET_FILE);
    }

    private class CustomTextViewListener implements TextView.OnEditorActionListener{
        @Override
        public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
            if(actionId == EditorInfo.IME_ACTION_DONE){
                parseBounds();
            }
            return false;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == REQUEST_GET_FILE && resultCode == Activity.RESULT_OK){
            Log.i(LearningActivity.TAG, "resultCode: " + resultCode);
            if(data == null) {
                Log.e(LearningActivity.TAG, "no file chosen");
                return;
            }
            Uri uri = null;
            try{uri = data.getData();} catch (Exception e) {
                Log.i(LearningActivity.TAG, e.getLocalizedMessage());
            }

            if(uri == null){
                Log.e(LearningActivity.TAG, "no Uri");
                return;
            }

            try {
                wordListModel = new ListModel(this, uri);
            }
            catch (FileNotFoundException e){
                Log.e(LearningActivity.TAG, null, e);
                setResult(Activity.RESULT_CANCELED, null);
                finish();
                return;
            }
            setTitle(wordListModel.getFilename());
            textStart.setText(String.valueOf(wordListModel.getStart() + 1));
            textEnd.setText(String.valueOf(wordListModel.getEnd()));
            textTotal.setText(String.valueOf(wordListModel.getTotal()));
            textFirst.setText(wordListModel.getFirst().kanji);
            textLast.setText(wordListModel.getLast().kanji);
        }else{
            setResult(Activity.RESULT_CANCELED, null);
            finish();
        }
    }

    public void parseBounds(){

        int start=0, end = wordListModel.getTotal();

        try{start = Integer.parseInt(textStart.getText().toString())-1;
        }catch(Exception e) {/*pass*/}

        try{end = Integer.parseInt(textEnd.getText().toString());}
        catch(Exception e) {/*pass*/}

        //wpisanie indeksów wyświetlanych kanji
        textStart.setText(String.valueOf(wordListModel.setStart(start)+1));
        textEnd.setText(String.valueOf(wordListModel.setEnd(end)));

        //wyświetlenie samych kanji
        textFirst.setText(wordListModel.getFirst().kanji);
        textLast.setText(wordListModel.getLast().kanji);

        //dopasowanie rozmiaru czcionki do okna
        int length = wordListModel.getFirst().kanji.length();
        if(length < 5)
            textFirst.setTextSize(40);
        else {
            Configuration conf = getResources().getConfiguration();
            int screenWidth = conf.screenWidthDp;
            textFirst.setTextSize(screenWidth*4/9/length);
        }

        textLast.setText(wordListModel.getLast().kanji);
        length = wordListModel.getLast().kanji.length();
        if(length < 5)
            textLast.setTextSize(40);
        else {
            Configuration conf = getResources().getConfiguration();
            int screenWidth = conf.screenWidthDp;
            textLast.setTextSize(screenWidth*4/9/length);
        }
    }

    public void cancel(View view){
        setResult(Activity.RESULT_CANCELED, null);
        finish();
    }

    public void ok(View view){
        parseBounds();
        Uri uri = null;
        try {
            uri = wordListModel.saveData();
        }catch(Exception e){
            Log.e(LearningActivity.TAG, "List Model: ", e);
            setResult(Activity.RESULT_CANCELED, null);
            finish();
        }
        Intent data = new Intent();
        data.setData(uri);
        setResult(Activity.RESULT_OK, data);
        finish();
    }
}
