package desu.nihongo;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.io.FileNotFoundException;

public class GetReviewActivity extends AppCompatActivity {

    DBHandler handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_review);

        TextView textLearning = findViewById(R.id.textLearning);
        TextView textReview = findViewById(R.id.textReview);
        TextView textRepeat = findViewById(R.id.textRepeat);


        handler = new SQLiteHelper(this, null);

        textLearning.append(" (" + handler.getWriteLearningCount() + ")");
        textReview.append(" (" + handler.getWriteReviewCount() + ")");
        textRepeat.append(" (" + handler.getWriteRepeatCount() + ")");
    }

    public void cancel(View view){
        setResult(Activity.RESULT_CANCELED, null);
        finish();
    }

    public void ok(View view){
        EditText textLearningEdit = findViewById(R.id.textLearningEdit);
        EditText textReviewEdit = findViewById(R.id.textReviewEdit);
        EditText textRepeatEdit = findViewById(R.id.textRepeatEdit);

        int learning = 0, review = 0, repeat = 0;
        try{learning = Integer.parseInt(textLearningEdit.getText().toString());}
        catch(Exception e){}
        try{review = Integer.parseInt(textReviewEdit.getText().toString());}
        catch(Exception e){}
        try{repeat = Integer.parseInt(textRepeatEdit.getText().toString());}
        catch(Exception e){}

        Uri currentUri = getIntent().getData();
        ListModel currentList;
        try {
            currentList = new ListModel(this, currentUri);
        } catch (FileNotFoundException e) {
            currentList = new ListModel(this);
        }

        ListModel newList = ListModel.getReview(
                this,new SQLiteHelper(this, null),
                learning, review, repeat, currentList);

        newList.setStart(0);
        newList.setEnd(Integer.MAX_VALUE);

        Uri uri = null;
        try {
            uri = newList.saveData();
        }catch(Exception e){
            Log.e(LearningActivity.TAG, "List Model: ", e);
            setResult(Activity.RESULT_CANCELED, null);
            finish();
        }
        Intent data = new Intent();
        data.setData(uri);
        setResult(Activity.RESULT_OK, data);
        finish();
    }
}
