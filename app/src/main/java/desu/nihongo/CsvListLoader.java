package desu.nihongo;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;

public class CsvListLoader implements ListLoader {

    public  void save(OutputStream file, List<Word> wordList) throws IOException {
        OutputStreamWriter writer = new OutputStreamWriter(file, StandardCharsets.UTF_8);
        boolean isFirst = true;
        for(Word word : wordList) {
            if(isFirst)
                isFirst = false;
            else
                writer.write("\r\n");
            writer.write(toCsv(word));
        }
        writer.flush();
        writer.close();
    }

    public LinkedList<Word> load(InputStream file){
        LinkedList<Word> list = new LinkedList<>();
        Scanner scanner = new Scanner(file, "UTF-8");
        while(scanner.hasNext()) {
            String line = scanner.nextLine();
            Word word = fromCsv(line);
            if(word != null)
                list.add(word);
        }
        scanner.close();
        return list;
    }

    private static String toCsv(Word word) {
        return String.format(Locale.getDefault(), "%d;%s;%s;%s;%d;%d;%d;%d",
                word.index,
                word.kanji,
                word.reading,
                word.english,
                word.scoreRead,
                word.scoreWrite,
                word.seen ? 1 : 0,
                word.forReview ? 1 : 0);
    }

    private static Word fromCsv(String line) {
        Word word = new Word();
        String[] fields = line.split(";", 9);
        if(fields.length < 8)
            return null;

        try {word.index = Integer.valueOf(fields[0]);} catch(NumberFormatException e) {}
        word.kanji = fields[1];
        word.reading = fields[2];
        word.english = fields[3];
        try {word.scoreRead = Integer.valueOf(fields[4]);}catch(NumberFormatException e) {}
        try {word.scoreWrite = Integer.valueOf(fields[5]);}catch(NumberFormatException e) {}
        try {word.seen = Boolean.parseBoolean(fields[6]);}catch(NumberFormatException e) {}
        try {word.forReview = Boolean.parseBoolean(fields[7]);}catch(NumberFormatException e) {}
        return word;
    }
}
