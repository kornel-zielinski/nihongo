package desu.nihongo;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;

public class SetPropertyActivity extends AppCompatActivity {

    private EditText textValue;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_property);
        textValue = findViewById(R.id.textValue);
        textValue.setOnEditorActionListener((v, id, action)-> {
            if(id == EditorInfo.IME_ACTION_DONE){
                set(null);
            }
            return false;
        });
    }

    public void cancel(View view){
        setResult(Activity.RESULT_CANCELED, null);
        finish();
    }

    public void set(View view){
        Intent data = new Intent();
        int activityResult = Activity.RESULT_OK;
        try {
            data.putExtra("value", textValue.getText().toString());
        }catch (Exception e){activityResult = Activity.RESULT_CANCELED;}
        setResult(activityResult, data);
        finish();
    }
}