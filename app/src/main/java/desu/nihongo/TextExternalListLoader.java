package desu.nihongo;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class TextExternalListLoader implements ListLoader {

    public TextExternalListLoader(){
    }

    @Override
    public void save(OutputStream file, List<Word> wordList) throws IOException {
        OutputStreamWriter writer = new OutputStreamWriter(file, StandardCharsets.UTF_8);

        for (Word word : wordList){
            String message = String.format("%s %s %s\r\n", word.kanji, word.reading, word.english);
            writer.write(message);
        }
        writer.close();
    }

    @Override
    public LinkedList<Word> load(InputStream file){
        Scanner scanner = new Scanner(file, "UTF-8");
        LinkedList<Word> wordList = new LinkedList<>();

        Word temp;
        int i = 0;
        while(scanner.hasNext()) {
            temp = new Word();
            try{
                temp.kanji = scanner.next().trim();
                temp.reading = scanner.next().trim();
                temp.english = scanner.nextLine().trim();
            }
            catch (Exception e){continue;}
            temp.seen = false;
            temp.forReview = false;
            temp.index = i++;
            temp.scoreRead = 0;
            temp.scoreWrite = 0;
            wordList.add(temp);
        }
        scanner.close();
        return wordList;
    }
}
