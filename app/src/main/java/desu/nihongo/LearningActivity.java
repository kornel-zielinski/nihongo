package desu.nihongo;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Locale;

public class LearningActivity extends AppCompatActivity {

    public static final String TAG = "nihongo_debug";
    private static final String saveFile = "current_list.csv";
    private static final int REQUEST_GET_REVIEW = 101;
    private static final int REQUEST_GET_DATA_FROM_FILE = 102;
    private static final int REQUEST_SET_DELAY = 103;
    private static final int REQUEST_SET_BUTTON_DELAY = 104;


    private ListModel listModel;
    Boolean englishToKanji = false;
    long buttonDelay;

    TextView textStats;
    TextView textKanji;
    TextView textReading;
    TextView textEnglish;
    Button buttonNext;
    Button buttonAgain;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);
            listModel = new ListModel(this);
            listModel.addListChangedListener(() -> {
                setVisibilities();
                setTextFields();
                Log.i(LearningActivity.TAG, "List Changed");

            });
            listModel.addListEmptyListener(() -> {
                setContentView(R.layout.activity_empty);
                Log.i(LearningActivity.TAG, "List Empty");
            });
            Uri uri = null;
            if (savedInstanceState != null) {
                Log.i(LearningActivity.TAG, "Loading previous session");
                String uriString = savedInstanceState.getString("uri");
                uri = Uri.parse(uriString);
            } else {
                File path = getExternalFilesDir(null);
                File file = new File(path, LearningActivity.saveFile);
                uri = Uri.fromFile(file);
            }

            buttonDelay = getPreferences(Context.MODE_PRIVATE).getLong("button_delay", 1000);
            englishToKanji = getPreferences(Context.MODE_PRIVATE).getBoolean("english_to_kanji", false);
            listModel.setDelay(getPreferences(Context.MODE_PRIVATE).getInt("delay", 0));

            if (englishToKanji)
                setContentView(R.layout.activity_english);
            else
                setContentView(R.layout.activity_kanji);

            try {
                listModel.loadData(uri);
            } catch (FileNotFoundException e) {
                setContentView(R.layout.activity_empty);
                Log.e(LearningActivity.TAG, null, e);
            }
        }catch (Exception e){Log.e(LearningActivity.TAG, null, e);}
    }

    @Override
    protected void onPause() {
        listModel.updateDatabase(new SQLiteHelper(this, null));
        super.onPause();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        File path = getExternalFilesDir(null);
        File file = new File(path, LearningActivity.saveFile);
        Uri uri = Uri.fromFile(file);
        try {
            listModel.saveData(uri);
        }catch (Exception e){
            Log.e(LearningActivity.TAG, null, e);
            return;
        }
        outState.putString("uri", uri.toString());
        Log.i(LearningActivity.TAG, "LearningActivity: saved instance state");
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(resultCode == Activity.RESULT_CANCELED) {
            return;
        }
        switch(requestCode) {
            case REQUEST_GET_DATA_FROM_FILE:
                if (englishToKanji)
                    setContentView(R.layout.activity_english);
                else
                    setContentView(R.layout.activity_kanji);

                try {
                    listModel.loadData(data.getData());
                    listModel.writeToDatabase(new SQLiteHelper(this, null));
                } catch (FileNotFoundException e) {
                    Log.e(LearningActivity.TAG, null, e);
                }
                listModel.getLevelsFromDatabase(new SQLiteHelper(this, null));
                break;

            case REQUEST_GET_REVIEW:
                if (englishToKanji)
                    setContentView(R.layout.activity_english);
                else
                    setContentView(R.layout.activity_kanji);

                try {
                    listModel.loadData(data.getData());
                    listModel.writeToDatabase(new SQLiteHelper(this, null));
                } catch (FileNotFoundException e) {
                    Log.e(LearningActivity.TAG, null, e);
                }
                break;

            case REQUEST_SET_DELAY:
                listModel.setDelay(Integer.parseInt(data.getStringExtra("value")));
                getPreferences(MODE_PRIVATE).edit()
                        .putInt("delay", listModel.getDelay())
                        .apply();
                break;

            case REQUEST_SET_BUTTON_DELAY:
                buttonDelay = Long.parseLong(data.getStringExtra("value"));
                getPreferences(MODE_PRIVATE).edit()
                        .putLong("button_delay", buttonDelay)
                        .apply();
                break;

            default:
                Log.i(LearningActivity.TAG, "unrecognized request code");
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.learning, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem itemDelay = menu.findItem(R.id.setDelay);
        itemDelay.setTitle(String.format(Locale.getDefault(), "%s (%d)",
                getResources().getString(R.string.menu_change_delay),
                listModel.getDelay()));

        MenuItem itemButtonDelay = menu.findItem(R.id.setButtonDelay);
        itemButtonDelay.setTitle(String.format(Locale.getDefault(), "%s (%d ms)",
                getResources().getString(R.string.menu_change_button_delay),
                buttonDelay));

        MenuItem itemSetEnglish = menu.findItem(R.id.setEnglish);
        itemSetEnglish.setVisible(!englishToKanji);
        MenuItem itemSetKanji = menu.findItem(R.id.setKanji);
        itemSetKanji.setVisible(englishToKanji);

        MenuItem itemShowWordInfo = menu.findItem(R.id.showWordInfo);
        if(listModel.getTotal() == 0)
            itemShowWordInfo.setEnabled(false);
        else
            itemShowWordInfo.setEnabled(true);
        return true;
    }

    @Override
    public void onContentChanged() {
        super.onContentChanged();
        textStats = findViewById(R.id.textStats);
        textKanji = findViewById(R.id.textKanji);
        textReading = findViewById(R.id.textReading);
        textEnglish = findViewById(R.id.textEnglish);
        buttonNext = findViewById(R.id.button_next);
        buttonAgain = findViewById(R.id.button_again);
        Log.i(LearningActivity.TAG, "onContentChanged");
    }


    public void next(View view) {
        listModel.newKanji(true);
        if(buttonNext != null) {
            buttonNext.setEnabled(false);
            buttonNext.postDelayed(() -> {buttonNext.setEnabled(true);}, buttonDelay);
        }
    }

    public void again(View view) {
        listModel.newKanji(false);
        if(buttonAgain != null) {
            buttonAgain.setEnabled(false);
            buttonAgain.postDelayed(() -> {buttonAgain.setEnabled(true);}, buttonDelay);
        }
    }

    public void check(View view) {
        textKanji.setVisibility(View.VISIBLE);
        textReading.setVisibility(View.VISIBLE);
        textEnglish.setVisibility(View.VISIBLE);
    }

    public void loadDict(View view){
        loadDict();
    }

    public void getReview(View view){
        getReview();
    }


    public void setVisibilities(){
        if(englishToKanji){
            textKanji.setVisibility(View.INVISIBLE);
            textReading.setVisibility(View.INVISIBLE);
            textEnglish.setVisibility(View.VISIBLE);
        }else{
            textKanji.setVisibility(View.VISIBLE);
            textReading.setVisibility(View.INVISIBLE);
            textEnglish.setVisibility(View.INVISIBLE);
        }
    }

    public void setTextFields(){
        textStats.setText(String.valueOf(listModel.getTotal()));
        Word word = listModel.getFirst();
        textKanji.setText(word.kanji);
        textReading.setText(word.reading);
        textEnglish.setText(word.english);
    }

    public void getReview(){
        listModel.writeToDatabase(new SQLiteHelper(this, null));
        Intent intent = new Intent(this, GetReviewActivity.class);
        File path = getExternalFilesDir(null);
        File file = new File(path, LearningActivity.saveFile);
        Uri uri = Uri.fromFile(file);
        try {
            listModel.saveData(uri);
        }catch (Exception e){
            Log.e(LearningActivity.TAG, null, e);
            return;
        }
        intent.setData(uri);
        startActivityForResult(intent, REQUEST_GET_REVIEW);
    }

    public void loadDict(){
        listModel.writeToDatabase(new SQLiteHelper(this, null));
        Intent intent = new Intent(this, ChooseFileActivity.class);
        startActivityForResult(intent, REQUEST_GET_DATA_FROM_FILE);
    }


    public void menuLoadDict(MenuItem item){
        loadDict();
    }

    public void menuGetReview(MenuItem item){
        getReview();
    }

    public void menuSetDelay(MenuItem item){
        Intent intent = new Intent(this, SetPropertyActivity.class);
        startActivityForResult(intent, REQUEST_SET_DELAY);
    }

    public void menuSetButtonDelay(MenuItem item){
        Intent intent = new Intent(this, SetPropertyActivity.class);
        startActivityForResult(intent, REQUEST_SET_BUTTON_DELAY);
    }

    public void menuSetEnglish(MenuItem item){
        englishToKanji = true;
        getPreferences(MODE_PRIVATE).edit()
                .putBoolean("english_to_kanji", englishToKanji)
                .apply();

        if(listModel.getTotal() > 0) {
            setContentView(R.layout.activity_english);
            setVisibilities();
            setTextFields();
        }
    }

    public void menuSetKanji(MenuItem item){
        englishToKanji = false;
        getPreferences(MODE_PRIVATE).edit()
                .putBoolean("english_to_kanji", englishToKanji)
                .apply();
        if(listModel.getTotal() > 0) {
            setContentView(R.layout.activity_kanji);
            setVisibilities();
            setTextFields();
        }
    }

    public void menuClearList(MenuItem item){
        listModel.writeToDatabase(new SQLiteHelper(this, null));
        listModel.clear();
    }

    public void menuShowWordInfo(MenuItem item){
        try {
            int score = listModel.getFirst().scoreWrite;
            Toast.makeText(this,
                    getResources().getString(R.string.score) + score,
                    Toast.LENGTH_SHORT).show();
        }catch (Exception e){
            Log.e(LearningActivity.TAG, null, e);
        }
    }

    public void menuShowReviewInfo(MenuItem item){
        Intent intent = new Intent(this, TextViewActivity.class);
        intent.putExtra("text", listModel.getReviewInfo(new SQLiteHelper(this, null)));
        startActivity(intent);
    }

    public void menuShuffle(MenuItem item) {
        listModel.shuffle();
    }

    public void menuSort(MenuItem item) {
        listModel.sort();
    }

    public void menuSaveDatabase(MenuItem item){
        SQLiteHelper helper = new SQLiteHelper(this, null);
        File path = getExternalFilesDir(null);
        File file = new File(path, "db.txt");
        helper.saveList(file);
    }

    public void menuLoadDatabase(MenuItem item){
        SQLiteHelper helper = new SQLiteHelper(this, null);
        File path = getExternalFilesDir(null);
        File file = new File(path, "db.txt");
        helper.loadList(file);
    }

}
