package desu.nihongo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class TextViewActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_text_view);
        String text = getIntent().getStringExtra("text");
        TextView textDisplayed = findViewById(R.id.textDisplayed);
        textDisplayed.setText(text);
    }

    public void cancel(View view){
        finish();
    }
}
