package desu.nihongo;

import android.content.Context;
import android.net.Uri;
import android.webkit.MimeTypeMap;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

public class ListModel {
    public static final String saveFile = "save_file.csv";

    private int start = 0, end = 0, delay = 0;
    private String filename = "";
    private LinkedList<Word> wordList;
    private LinkedList<Word> updateList;
    private Context context;

    private LinkedList<EventListener> emptyListeners;
    private LinkedList<EventListener> changedListeners;

    public interface EventListener extends java.util.EventListener{
        void onEvent();
    }

    public ListModel(Context context){
        this.context = context;
        wordList = new LinkedList<>();
        updateList = new LinkedList<>();
        emptyListeners = new LinkedList<>();
        changedListeners = new LinkedList<>();
    }

    public ListModel (Context context, Uri uri) throws FileNotFoundException{
        this.context = context;
        wordList = new LinkedList<>();
        updateList = new LinkedList<>();
        emptyListeners = new LinkedList<>();
        changedListeners = new LinkedList<>();

        loadData(uri);
    }

    public int setStart(int start) {
        if(start < 0)
            this.start = 0;
        else if(start >= wordList.size())
            this.start = wordList.size();
        else
            this.start = start;
        return this.start;
    }

    public int setEnd(int end) {
        if(end < 0)
            this.end = 0;
        else if(end >= wordList.size())
            this.end = wordList.size();
        else
            this.end = end;
        return this.end;
    }

    public void setDelay(int delay) {
        this.delay = delay;
    }

    public int getStart(){return start;}

    public int getEnd() {return end;}

    public int getDelay() {return delay;}

    public int getTotal() {return wordList.size();}

    public int getTotalUniqueKanji(){
        LinkedList<Character> charList = new LinkedList<>();
        for(Word word : wordList){
            for(int i=0; i<word.kanji.length(); ++i){
                char ji = word.kanji.charAt(i);
                if(ji >= '\u4e00' && ji <= '\u9fff'){
                    if(!charList.contains(ji))
                        charList.add(ji);
                }
            }
        }
        return charList.size();
    }

    public String getFilename() {return filename;}

    public Word getFirst(){
        return wordList.get(start);
    }

    public Word getLast(){
        return wordList.get(end-1);
    }

    public void newKanji(Boolean skip){
        Word current = wordList.peekFirst();

        if(!current.seen){
            if(skip){
                if(current.scoreWrite < SQLiteHelper.TOTAL_LEVELS)
                    current.scoreWrite++;
                current.forReview = false;
            }
            else{
                if(current.scoreWrite <= 1)
                    current.scoreWrite = 1;
                else
                    current.scoreWrite--;
                current.forReview = true;
            }
            current.seen = true;
            updateList.add(current);
        }
        if (skip) {
            try {
                wordList.pop();
            } catch (Exception e) {
                onListEmpty();
                return;
            }
        }
        else if (delay == 0 || delay > wordList.size() - 1)
            wordList.add(wordList.pop());
        else
            wordList.add(delay, wordList.pop());
        if(wordList.isEmpty())
            onListEmpty();
        else
            onListChanged();
    }

    public void shuffle(){
        Collections.shuffle(wordList);
        onListChanged();
    }

    public void sort(){
        Collections.sort(wordList);
        onListChanged();
    }

    public void clear(){
        wordList.clear();
        onListEmpty();
    }

    public void loadData(Uri uri) throws FileNotFoundException{
        String extension = MimeTypeMap.getFileExtensionFromUrl(uri.toString());
        ListLoader loader;
        switch(extension.toLowerCase()){
            case "txt":
                loader = new TextExternalListLoader();
                break;
            case "csv":
                loader = new CsvListLoader();
                break;
            default:
                loader = new TextExternalListLoader();
                break;
        }
        LinkedList<Word> wordList = loader.load(context.getContentResolver().openInputStream(uri));
        this.wordList.addAll(wordList);
        if(this.wordList.isEmpty()) {
            filename = null;
            onListEmpty();
        }
        else {
            String[] fragments = uri.getLastPathSegment().split("/");
            filename = fragments[fragments.length-1];
        }
        start = 0;
        end = this.wordList.size();
        if(!wordList.isEmpty())
            onListChanged();
    }

    public void saveData(Uri uri) throws IOException {
        setStart(start);
        setEnd(end);
        String extension = MimeTypeMap.getFileExtensionFromUrl(uri.toString());
        ListLoader loader;
        switch(extension.toLowerCase()){
            case "txt":
                loader = new TextExternalListLoader();
                break;
            case "csv":
                loader = new CsvListLoader();
                break;
            default:
                loader = new TextExternalListLoader();
                break;
        }
        loader.save(context.getContentResolver().openOutputStream(uri), wordList.subList(start, end));
    }

    public Uri saveData() throws IOException{
        File path = context.getExternalFilesDir(null);
        File file = new File(path, ListModel.saveFile);
        Uri uri = Uri.fromFile(file);
        saveData(uri);
        return uri;
    }

    public void add(Iterable<Word> wordList){
        for(Word word : wordList)
            this.wordList.add(word);
        onListChanged();
    }

    public boolean contains(Word word){
        for(Word comparedWord : wordList){
            if(word.kanji.equals(comparedWord.kanji))
                return true;
        }
        return false;
    }

    public void add(Word word){
        this.wordList.add(word);
        onListChanged();
    }

    public void updateDatabase(DBHandler handler){
        handler.update(updateList);
        updateList.clear();
    }

    public void writeToDatabase(DBHandler handler){
        handler.write(wordList);
    }

    public void getLevelsFromDatabase(DBHandler handler){
        for (Word word : wordList) {
            word.scoreWrite = handler.getScore(word);
        }
        for (Word word : updateList) {
            word.scoreWrite = handler.getScore(word);
        }
    }

    public String getReviewInfo(DBHandler handler){

        ListModel totalModel = new ListModel(context);
        totalModel.wordList = (LinkedList<Word>) handler.getAll();

        StringBuilder text = new StringBuilder();
        text.append("Total: ").append(totalModel.getTotal()).append("\r\n");
        text.append("Unique Kanji: ").append(totalModel.getTotalUniqueKanji()).append("\r\n\r\n");
        int a, b;
        for (int i = 0; i <= SQLiteHelper.TOTAL_LEVELS; i++){
            a = handler.howManyWithScore(i);
            b = handler.howManyReadyWithScore(i);
            text.append(String.format(Locale.getDefault(), "score %2d: %4d (%d)\r\n", i, a, b));
        }
        return text.toString();
    }

    public static ListModel getReview(Context context, DBHandler handler, int learning, int review, int repeat, ListModel currentList){
        if(currentList == null)
            currentList = new ListModel(context);

        int currentTotal = currentList.getTotal();
        ListModel newList = new ListModel(context);
        List<Word> list = handler.getWriteLearningList(learning + currentTotal);
        int finalSize = Math.min(list.size(), learning);
        for(Word word : list){
            if(!currentList.contains(word))
                newList.add(word);
            if(newList.getTotal() >= finalSize)
                break;
        }

        list = handler.getWriteReviewList(review + currentTotal);
        finalSize = newList.getTotal() + Math.min(list.size(), review);
        for(Word word : list){
            if(!currentList.contains(word))
                newList.add(word);
            if(newList.getTotal() >= finalSize)
                break;
        }

        list = handler.getWriteRepeatList(repeat + review + currentTotal);
        finalSize = newList.getTotal() + Math.min(list.size(), repeat);
        for(Word word : list){
            if(!currentList.contains(word) && !newList.contains(word))
                newList.add(word);
            if(newList.getTotal() >= finalSize)
                break;
        }
        return newList;
    }

    private void onListEmpty(){
        for(EventListener listener : emptyListeners)
            listener.onEvent();
    }

    private void onListChanged(){
        for(EventListener listener : changedListeners)
            listener.onEvent();
    }

    public void addListEmptyListener(EventListener listener){
        if(!emptyListeners.contains(listener))
            emptyListeners.add(listener);
    }

    public void addListChangedListener(EventListener listener){
        if(!changedListeners.contains(listener))
            changedListeners.add(listener);
    }

    public void removeListEmptyListener(EventListener listener){
        emptyListeners.remove(listener);
    }

    public void removeListChangedListener(EventListener listener){
        changedListeners.remove(listener);
    }
}
