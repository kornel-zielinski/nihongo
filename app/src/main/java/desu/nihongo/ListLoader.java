package desu.nihongo;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.LinkedList;
import java.util.List;

public interface ListLoader {
    public void save(OutputStream file, List<Word> wordList) throws IOException;
    public LinkedList<Word> load(InputStream file);
}
