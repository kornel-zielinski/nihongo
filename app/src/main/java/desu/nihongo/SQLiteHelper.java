package desu.nihongo;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class SQLiteHelper extends SQLiteOpenHelper implements DBHandler {

    public static final String DATABASE_NAME = "Database";
    public static final int DATABASE_VERSION = 5;
    public static final int WEEK = 604800;
    public static final int DAY = 86400;
    public static final int LEARNING_LEVELS = 5;
    public static final int TOTAL_LEVELS = 15;
    public static final int[] TIME_LIMITS = {DAY,3*DAY,WEEK,2*WEEK,3*WEEK,4*WEEK,6*WEEK,8*WEEK,10*WEEK,12*WEEK};
    public static final double[] WEIGHTS = {0.15, 0.15, 0.15, 0.1, 0.1, 0.1, 0.1, 0.05, 0.05, 0.05};

    public SQLiteHelper(Context context, SQLiteDatabase.CursorFactory factory) {
        super(context, DATABASE_NAME, factory, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        /*String query = "CREATE TABLE words (kanji TEXT PRIMARY KEY ON CONFLICT IGNORE, reading TEXT, " +
                "english TEXT, last_seen BIGINT, score_read SMALLINT, score_write SMALLINT, " +
                "for_write_review BOOLEAN, for_read_review BOOLEAN)";*/
        String query = "CREATE TABLE words (kanji TEXT PRIMARY KEY ON CONFLICT IGNORE, reading TEXT, " +
                "english TEXT, last_seen BIGINT, score_read SMALLINT DEFAULT 0, score_write SMALLINT DEFAULT 0, " +
                "for_write_review BOOLEAN DEFAULT 0, for_read_review BOOLEAN DEFAULT 0)";
        sqLiteDatabase.execSQL(query);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        String query = "DROP TABLE IF EXISTS words";
        sqLiteDatabase.execSQL(query);
        onCreate(sqLiteDatabase);
    }

    public List<Word> getAll(){
        SQLiteDatabase database = getReadableDatabase();
        Cursor cursor = database.query("words", null, null, null, null, null, null);

        LinkedList<Word> list = new LinkedList<Word>();
        while(cursor.moveToNext()){
            Word word = new Word();
            word.kanji = cursor.getString(0);
            word.reading = cursor.getString(1);
            word.english = cursor.getString(2);
            word.index = cursor.getInt(3);
            list.add(word);
        }
        cursor.close();
        return list;
    }

    public long write(Word word){
        SQLiteDatabase database = getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("kanji", word.kanji);
        contentValues.put("reading", word.reading);
        contentValues.put("english", word.english);
        contentValues.put("last_seen", System.currentTimeMillis()/1000);
        contentValues.put("score_read", 0);
        contentValues.put("score_write", 0);

        return database.insert("words", null, contentValues);
    }

    public void write(Iterable<Word> list){
        SQLiteDatabase database = getWritableDatabase();
        database.beginTransaction();
        ContentValues contentValues = null;
        for(Word word: list) {
            contentValues = new ContentValues();
            contentValues.put("kanji", word.kanji);
            contentValues.put("reading", word.reading);
            contentValues.put("english", word.english);
            contentValues.put("last_seen", System.currentTimeMillis()/1000);
            contentValues.put("score_read", 0);
            contentValues.put("score_write", 0);

            database.insert("words", null, contentValues);
        }
        database.setTransactionSuccessful();
        database.endTransaction();
    }

    public int update(Word word){
        SQLiteDatabase database = getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("last_seen", System.currentTimeMillis()/1000);
        contentValues.put("score_read", word.scoreRead);
        contentValues.put("score_write", word.scoreWrite);
        contentValues.put("for_write_review", word.forReview);

        return database.update("words", contentValues, "kanji LIKE ?", new String[]{word.kanji});
    }

    public void update(Iterable<Word> list){
        SQLiteDatabase database = getWritableDatabase();
        database.beginTransaction();
        ContentValues contentValues = new ContentValues();
        for(Word word: list) {
            contentValues.put("last_seen", System.currentTimeMillis() / 1000);
            contentValues.put("score_read", word.scoreRead);
            contentValues.put("score_write", word.scoreWrite);
            contentValues.put("for_write_review", word.forReview);
            database.update("words", contentValues, "kanji LIKE ?", new String[] {word.kanji});
        }
        database.setTransactionSuccessful();
        database.endTransaction();
    }

    public int modify(String kanjiToUpdate, Word word){
        SQLiteDatabase database = getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("last_seen", System.currentTimeMillis()/1000);
        contentValues.put("reading", word.reading);
        contentValues.put("english", word.english);
        contentValues.put("kanji", word.kanji);

        return database.update("words", contentValues, "kanji LIKE ?", new String[]{kanjiToUpdate});
    }

    public int delete(Word word){
        SQLiteDatabase database = getWritableDatabase();
        return database.delete("words", "kanji LIKE ?", new String[]{word.kanji});
    }

    public void setScore(Iterable<Word> list, int score){
        int offset = 0;

        if(score > LEARNING_LEVELS && score <= TOTAL_LEVELS)
            offset = TIME_LIMITS[score-LEARNING_LEVELS-1];
        SQLiteDatabase database = getWritableDatabase();
        database.beginTransaction();
        ContentValues contentValues = new ContentValues();
        for(Word word: list) {
            contentValues.put("last_seen", System.currentTimeMillis() / 1000 - offset);
            contentValues.put("score_read", word.scoreRead);
            contentValues.put("score_write", word.scoreWrite);
            contentValues.put("for_write_review", word.forReview);
            database.update("words", contentValues, "kanji LIKE ?", new String[] {word.kanji});
        }
        database.setTransactionSuccessful();
        database.endTransaction();
    }

    public List<Word> getWriteLearningList(int limit){
        SQLiteDatabase database = getReadableDatabase();
        LinkedList<Word> list = new LinkedList<Word>();
        int now = (int) (System.currentTimeMillis()/1000);

        Cursor cursor = database.query("words",
                new String[]{"kanji", "reading", "english", "score_read", "score_write"},
                "score_write BETWEEN 1 AND ? AND last_seen < ?",
                new String[] {String.valueOf(LEARNING_LEVELS), String.valueOf(now-DAY/8)},
                null, null, "last_seen", String.valueOf(limit));


        while(cursor.moveToNext()){
            Word word = new Word();
            word.kanji = cursor.getString(0);
            word.reading = cursor.getString(1);
            word.english = cursor.getString(2);
            word.scoreRead = cursor.getInt(3);
            word.scoreWrite = cursor.getInt(4);
            list.add(word);
        }
        cursor.close();

        return list;
    }

    public List<Word> getWriteReviewList(int limit){
        SQLiteDatabase database = getReadableDatabase();
        LinkedList<Word> list = new LinkedList<Word>();
        int now = (int) (System.currentTimeMillis()/1000);

        int reviewLevels = TOTAL_LEVELS - LEARNING_LEVELS;
        int[] wordsAvailable = new int[reviewLevels];
        int[] wordsToGet = new int[reviewLevels];
        Cursor cursor;

        for(int i = 0; i < reviewLevels; i++) {
            cursor = database.rawQuery("SELECT COUNT(*) FROM words WHERE score_write = ? AND last_seen < ?",
                    new String[] {String.valueOf(LEARNING_LEVELS + 1 + i), String.valueOf(now - TIME_LIMITS[i])});
            cursor.moveToFirst();
            wordsAvailable[i] = cursor.getInt(0);
            cursor.close();
        }

        if(sum(wordsAvailable) > limit) {
            while (sum(wordsToGet) < limit) {
                for (int i = 0; i < reviewLevels && sum(wordsToGet) < limit; i++) {
                    int newval = (int) Math.ceil((limit - sum(wordsToGet)) * WEIGHTS[i]);
                    wordsToGet[i] = Math.min(wordsToGet[i] + newval, wordsAvailable[i]);
                }
            }
        }
        else
            wordsToGet = wordsAvailable;

        for (int i = 0; i < reviewLevels; i++) {
            cursor = database.query("words",
                    new String[]{"kanji", "reading", "english", "score_read", "score_write"},
                    "for_write_review = 0 AND score_write = ? AND last_seen < ?",
                    new String[]{String.valueOf(LEARNING_LEVELS + 1 + i), String.valueOf(now - TIME_LIMITS[i])},
                    null, null, "last_seen", String.valueOf(wordsToGet[i]));
            while (cursor.moveToNext()) {
                Word word = new Word();
                word.kanji = cursor.getString(0);
                word.reading = cursor.getString(1);
                word.english = cursor.getString(2);
                word.scoreRead = cursor.getInt(3);
                word.scoreWrite = cursor.getInt(4);
                list.add(word);
            }
            cursor.close();
        }

        return list;
    }

    public List<Word> getWriteRepeatList(int limit) {
        SQLiteDatabase database = getReadableDatabase();
        LinkedList<Word> list = new LinkedList<Word>();
        int now = (int) (System.currentTimeMillis()/1000);

        Cursor cursor = database.query("words",
                new String[]{"kanji", "reading", "english", "score_read", "score_write"},
                "for_write_review = 1 AND score_write BETWEEN ? AND ? AND last_seen < ?",
                new String[] {String.valueOf(LEARNING_LEVELS+1), String.valueOf(TOTAL_LEVELS),
                        String.valueOf(now-DAY/2)}, null, null, "last_seen", String.valueOf(limit));

        while(cursor.moveToNext()){
            Word word = new Word();
            word.kanji = cursor.getString(0);
            word.reading = cursor.getString(1);
            word.english = cursor.getString(2);
            word.scoreRead = cursor.getInt(3);
            word.scoreWrite = cursor.getInt(4);
            list.add(word);
        }
        cursor.close();

        return list;
    }

    public int getWriteLearningCount(){
        int now = (int) (System.currentTimeMillis()/1000);
        SQLiteDatabase database = getReadableDatabase();
        Cursor cursor = database.rawQuery("SELECT COUNT(*) FROM words WHERE score_write BETWEEN 1 AND ? AND last_seen < ?",
                new String[]{String.valueOf(LEARNING_LEVELS), String.valueOf(now-DAY/8)});
        cursor.moveToFirst();
        int result = cursor.getInt(0);
        cursor.close();
        return result;
    }

    public int getWriteReviewCount(){
        SQLiteDatabase database = getReadableDatabase();
        int now = (int) (System.currentTimeMillis()/1000);

        int reviewLevels = TOTAL_LEVELS - LEARNING_LEVELS;
        int[] wordsAvailable = new int[reviewLevels];

        Cursor cursor;
        for(int i = 0; i < reviewLevels; i++) {
            cursor = database.rawQuery("SELECT COUNT(*) FROM words WHERE score_write = ? AND last_seen < ?",
                    new String[] {String.valueOf(LEARNING_LEVELS + 1 + i), String.valueOf(now - TIME_LIMITS[i])});
            cursor.moveToFirst();
            wordsAvailable[i] = cursor.getInt(0);
            cursor.close();
        }
        return sum(wordsAvailable);
    }

    public int getWriteRepeatCount(){
        int now = (int) (System.currentTimeMillis()/1000);
        SQLiteDatabase database = getReadableDatabase();
        Cursor cursor = database.rawQuery("SELECT COUNT(*) FROM words WHERE for_write_review = 1 AND score_write BETWEEN ? AND ? AND last_seen < ?",
                new String[]{String.valueOf(LEARNING_LEVELS+1), String.valueOf(TOTAL_LEVELS), String.valueOf(now-DAY/2)});
        cursor.moveToFirst();
        int result = cursor.getInt(0);
        cursor.close();
        return result;
    }

    public int size(){
        SQLiteDatabase database = getReadableDatabase();
        Cursor cursor = database.rawQuery("SELECT COUNT(*) FROM words", null);
        cursor.moveToFirst();
        int result = cursor.getInt(0);
        cursor.close();
        return result;
    }

    public int howManyWithScore(int score){
        SQLiteDatabase database = getReadableDatabase();
        Cursor cursor = database.rawQuery("SELECT COUNT(*) FROM words WHERE score_write = " + String.valueOf(score), null);
        cursor.moveToFirst();
        int result = cursor.getInt(0);
        cursor.close();
        return result;
    }

    public int howManyReadyWithScore(int score){
        int timestamp = (int) (System.currentTimeMillis()/1000);
        if(score > 0 && score <= LEARNING_LEVELS)
            timestamp -= DAY/8;
        else if(score > LEARNING_LEVELS && score <= TOTAL_LEVELS)
            timestamp -= TIME_LIMITS[score-LEARNING_LEVELS-1];
        SQLiteDatabase database = getReadableDatabase();
        Cursor cursor = database.rawQuery("SELECT COUNT(*) FROM words WHERE score_write = ? AND last_seen < ?",
                new String[] {String.valueOf(score), String.valueOf(timestamp)});
        cursor.moveToFirst();
        int result = cursor.getInt(0);
        cursor.close();
        return result;
    }

    public int getScore(Word word){
        SQLiteDatabase database = getReadableDatabase();
        Cursor cursor = database.rawQuery("SELECT score_write FROM words WHERE kanji LIKE ?", new String[]{word.kanji});
        cursor.moveToFirst();
        int result = cursor.getInt(0);
        cursor.close();
        return result;
    }

    public void drop(){
        SQLiteDatabase database = getReadableDatabase();
        onUpgrade(database, 0, 0);
    }

    public void fakeData(){
        drop();
        SQLiteDatabase database = getWritableDatabase();

        for(int i = 0; i<100; i++){
            Word word = new Word(String.format("K%d",i), String.format("R%d",i), String.format("E%d",i));
            word.index = i;

            ContentValues contentValues = new ContentValues();
            contentValues.put("kanji", word.kanji);
            contentValues.put("reading", word.reading);
            contentValues.put("english", word.english);
            contentValues.put("last_seen", System.currentTimeMillis()/1000-DAY*i);
            contentValues.put("score_read", i%16);
            contentValues.put("score_write", i%16);
            contentValues.put("for_read_review", 0);
            contentValues.put("for_write_review", 0);

            database.insert("words", null, contentValues);
        }
    }

    public void loadList(File file ) {
        if(file == null)
            return;

        drop();
        Scanner scanner = null;
        try {scanner = new Scanner(file, "UTF-8");} catch (Exception e) {
            return;
        }

        Word temp;
        long timestamp;
        SQLiteDatabase database = getWritableDatabase();
        database.beginTransaction();
        ContentValues contentValues = new ContentValues();
        while(scanner.hasNext()) {
            temp = new Word();
            try{
                temp.scoreWrite = Integer.valueOf(scanner.next().trim());
                timestamp = Long.valueOf(scanner.next().trim());
                temp.kanji = scanner.next().trim();
                temp.reading = scanner.next().trim();
                temp.english = scanner.nextLine().trim();}
            catch (Exception e){
                e.printStackTrace();
                continue;
            }

            contentValues.put("kanji", temp.kanji);
            contentValues.put("reading", temp.reading);
            contentValues.put("english", temp.english);
            contentValues.put("last_seen", timestamp);
            contentValues.put("score_read", 0);
            contentValues.put("score_write", temp.scoreWrite);

            database.insert("words", null, contentValues);
        }
        scanner.close();

        database.setTransactionSuccessful();
        database.endTransaction();
    }

    public void saveList(File file){
        OutputStreamWriter writer;
        try{
            writer = new OutputStreamWriter(new FileOutputStream(file), StandardCharsets.UTF_8);
        }
        catch (Exception e){ return;}
        SQLiteDatabase database = getReadableDatabase();
        Cursor cursor = database.query("words", new String[]{"score_write", "last_seen", "kanji", "reading", "english"}, null, null, null, null, null);
        String message;
        int len = cursor.getColumnCount();

        while(cursor.moveToNext()){
            /*message = String.format("%d %d %s %s %s\r\n", cursor.getString(0),
                    cursor.getString(1), cursor.getString(2),
                    cursor.getString(3), cursor.getString(4));*/
            message="";
            for (int i=0; i<len; i++){
                message=message+cursor.getString(i)+" ";
            }
            //Log.i(MainActivity.TAG, message);
            try {writer.write(message + "\r\n");}
            catch (Exception e) {}
        }
        cursor.close();
        try{writer.close();}catch (Exception e){}

    }

    public static int sum(int[] array){
        int result = 0;
        for(int i : array)
            result += i;
        return result;
    }
}
