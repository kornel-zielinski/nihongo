package desu.nihongo;

import android.support.annotation.NonNull;

public class Word implements Comparable<Word>{
    public String kanji;
    public String reading;
    public String english;
    public int index;
    public int scoreRead;
    public int scoreWrite;
    public boolean seen;
    public boolean forReview;

    public Word(){
        super();
    }

    public Word(String kanji, String reading, String english){
        this.kanji = kanji;
        this.reading = reading;
        this.english = english;
    }

    @Override
    public int compareTo(@NonNull Word o) {
        if(index < o.index)
            return -1;
        else if (index == o.index)
            return 0;
        else
            return 1;
    }

    public String toString(){
        return String.format("%s %s %s\r\n", kanji, reading, english);
    }
}
