package desu.nihongo;

import java.util.List;

public interface DBHandler {

    public List<Word> getAll();

    public long write(Word word);

    public void write(Iterable<Word> list);

    public int update(Word word);

    public void update(Iterable<Word> list);

    public int modify(String kanjiToUpdate, Word word);

    public int delete(Word word);

    public void setScore(Iterable<Word> list, int score);

    public List<Word> getWriteLearningList(int limit);

    public List<Word> getWriteRepeatList(int limit);

    public List<Word> getWriteReviewList(int limit);

    public int getWriteLearningCount();

    public int getWriteReviewCount();

    public int getWriteRepeatCount();

    public int size();

    public int howManyWithScore(int score);

    public int howManyReadyWithScore(int score);

    public int getScore(Word word);

    public void drop();
}
